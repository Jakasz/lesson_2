public class Triangle {
    private double sideA;
    private double sideB;
    private double sideC;
    private String triangleName;

    public String getTriangleName() {
        return triangleName;
    }

    public void setTriangleName(String triangleName) {
        this.triangleName = triangleName;
    }

    public double getSideA() {
        return sideA;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideC(double sideC) {
        this.sideC = sideC;
    }

    public Triangle(double sideA, double sideB, double sideC, String triangleName) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
        this.triangleName = triangleName;
    }

    //Вычисление площади
    public void CalcArea(){
        double p = (sideA+sideB+sideC)/2; //периметр
        double s = Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC)); //площадь
        System.out.println("Area of "+triangleName+" is "+s);
    }

}
